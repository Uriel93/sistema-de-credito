export const environment = {
  production: true
};


export const properties = {
  login:{
    url: 'https://reqres.in/api/'
  },
  podemosApi: {
    url: 'http://localhost:4200',
    baseContext: '/podemos'
  },
};
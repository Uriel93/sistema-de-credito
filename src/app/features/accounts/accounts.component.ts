import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { userService } from 'src/app/core/services/user.service';
import { ModalPaymentComponent } from 'src/app/shared/modal-payment/modal-payment.component';
import { NotificationService } from 'src/app/shared/notifications-response/notifications-response.service';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.scss']
})
export class AccountsComponent implements OnInit {

  showcaldendar:boolean = false;
  showTransaction:boolean = false;
  selectedGrup:any;
  caldendar:any;
  trasaction:any;
  selectedAccount:any;

  dataAccount:any;
  displayedColumns: string[] = ['id', 'grupo', 'estatus', 'monto', 'saldo', 'opciones'];
  displayCalendar: string[] = ['id', 'cuenta', 'num_pago', 'monto', 'fecha_pago', 'estatus', 'transacciones'];
  displayTransaction: string[] = ['id', 'cuenta', 'fecha', 'monto'];

  constructor(
    private dialog: MatDialog,
    private user:userService,
    private mensaje: NotificationService
  ) { }

  ngOnInit(): void {
    this.allAccount();
  }

  showDetails(account:any){
    this.showcaldendar = false;
    this.showTransaction = false
    this.selectedGrup = account.grupo;
    this.getlistAccount(account.id)
    this.showcaldendar = !this.showcaldendar
  }

  detailsTrasaction(calendar:any){
    this.showTransaction = false
    this.selectedAccount = calendar.cuenta;
    this.getlistTrasaction(calendar.cuenta)
    this.showTransaction = !this.showTransaction
  }

  payAccount(account:any){
    const dialogRef = this.dialog.open(ModalPaymentComponent, {
      data: account
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.allAccount()
        this.getlistTrasaction(account.cuenta);
        this.getlistAccount(account.cuenta)
        this.mensaje.succes("Se realizo el pago");
      }
    });
  }

  allAccount(){
    this.user.getAllAccount().subscribe(
      (response) => { this.dataAccount = response; },
      (response) => { this.mensaje.error(response.error) }
    )
  }

  getlistAccount(accountId:any){
    this.caldendar = [];
    this.user.listCalendar(accountId).subscribe(
      (response) => { this.caldendar = response; },
      (response) => { this.mensaje.error(response.error) },
    )
  }

  getlistTrasaction(accountId:any){
    this.trasaction = [];
    this.user.lisTrasacition(accountId).subscribe(
      (response) => { this.trasaction = response; },
      (response) => { this.mensaje.error(response.error) },
    )
  }
}

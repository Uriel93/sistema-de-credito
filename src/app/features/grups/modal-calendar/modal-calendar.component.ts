import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { userService } from 'src/app/core/services/user.service';
import { ModalPaymentComponent } from 'src/app/shared/modal-payment/modal-payment.component';
import { NotificationService } from 'src/app/shared/notifications-response/notifications-response.service';

@Component({
  selector: 'app-modal-calendar',
  templateUrl: './modal-calendar.component.html',
  styleUrls: ['./modal-calendar.component.scss']
})
export class ModalCalendarComponent implements OnInit {

  nameModal:string = 'Calendario de pagos del grupo ' + this.data.grupo;
  caldendar:any;

  displayCalendar: string[] = ['id', 'cuenta', 'num_pago', 'monto', 'fecha_pago', 'estatus', 'transacciones'];

  constructor(
    public dialogRef: MatDialogRef<ModalCalendarComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private user:userService,
    private mensaje: NotificationService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.getlistAccount(this.data.id)
  }

  payAccount(account:any){
    const dialogRef = this.dialog.open(ModalPaymentComponent, {
      data: account
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.getlistAccount(account.cuenta)
        this.mensaje.succes("Se realizo el pago");
      }
    });
  }

  closeModal(){
    this.dialogRef.close(true);
  }

  getlistAccount(accountId:any){
    this.caldendar = [];
    this.user.listCalendar(accountId).subscribe(
      (response) => { this.caldendar = response; },
      (response) => { this.mensaje.error(response.error) },
    )
  }

}

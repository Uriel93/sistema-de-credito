import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { userService } from 'src/app/core/services/user.service';
import { NotificationService } from 'src/app/shared/notifications-response/notifications-response.service';

@Component({
  selector: 'app-modal-add-client',
  templateUrl: './modal-add-client.component.html',
  styleUrls: ['./modal-add-client.component.scss']
})
export class ModalAddClientComponent implements OnInit {

  nameModal:string = 'Agregar Cliente';
  allClient:any;
  selectedCheck:any[] = [];

  constructor(
    public dialogRef: MatDialogRef<ModalAddClientComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private mensaje: NotificationService,
    private serviceUser: userService
  ) { }

  ngOnInit(): void {
    this.getallClient();
    this.data.listClient.map((searchId:any) => {
      this.selectedCheck.push(searchId.id);
    })
  }

  addClient(){
    let arrayClient:any[] = [];
    let sendClient:any;
    this.selectedCheck.map((setData:any) => {
      arrayClient.push({ id: setData })
    })
    sendClient = { data: arrayClient }
    this.saveClient(this.data.idGroup, sendClient)
  }

  saveClient(groupId:any, request:any){
    this.serviceUser.addClientAGroup(groupId, request).subscribe(
      () => { this.dialogRef.close(true); },
      (response) => { this.mensaje.warning(response.error) }
    )
  }

  getallClient(){
    this.serviceUser.getUsers().subscribe(
      (response) => { this.allClient = response },
      (response) => { this.mensaje.error(response.error) }
    )
  }

}

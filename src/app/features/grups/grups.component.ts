import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { userService } from 'src/app/core/services/user.service';
import { NotificationService } from 'src/app/shared/notifications-response/notifications-response.service';
import { ModalAddAccountComponent } from './modal-add-account/modal-add-account.component';
import { ModalAddClientComponent } from './modal-add-client/modal-add-client.component';
import { ModalCalendarComponent } from './modal-calendar/modal-calendar.component';
import { ModalsGrupsComponent } from './modals-grup/modals.component';

@Component({
  selector: 'app-grups',
  templateUrl: './grups.component.html',
  styleUrls: ['./grups.component.scss']
})
export class GrupsComponent implements OnInit {

  dataGroups:any;
  listClient:any;
  account:any;
  selectedAccount:any;
  showList:boolean = false;
  groupId:any;

  displayedColumnsGrups: string[] = ['id', 'nombre', 'opciones'];
  displayedColumns: string[] = ['id', 'grupo', 'estatus', 'monto', 'saldo', 'opciones'];

  constructor(
    private user:userService,
    private mensaje: NotificationService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.allGroup(); 
  }

  createGroup(){
    const dialogRef = this.dialog.open(ModalsGrupsComponent);
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.allGroup();
      }
    });
  }

  editGroup(grup:any){
    const dialogRef = this.dialog.open(ModalsGrupsComponent, {
      data: grup
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.allGroup();
      }
    });
  }

  detailGroup(grup:any){
    this.showList = false
    this.selectedAccount = grup.nombre;
    this.groupId = grup.id
    this.getlistClient(grup.id)
    this.showList = !this.showList
  }

  addClient(){
    const dialogRef = this.dialog.open(ModalAddClientComponent, {
      data: { listClient: this.listClient, idGroup: this.groupId }
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.getlistClient(this.groupId);
        this.mensaje.succes("Se agrego con exito.")
      }
    });
  }

  addAccount(){
    const dialogRef = this.dialog.open(ModalAddAccountComponent, {
      data: this.groupId
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.getlistClient(this.groupId);
        this.mensaje.succes("Se agrego con exito.")
      }
    });
  }

  calendarAccount(account:any){
    const dialogRef = this.dialog.open(ModalCalendarComponent, {
      data: account
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.getlistClient(this.groupId);
      }
    });
  }

  allGroup(){
    this.user.getGroup().subscribe(
      (response) => { this.dataGroups = response },
      (response) => { this.mensaje.error(response.error) }
    )
  }

  getlistClient(grupId:any){
    this.listClient = [];
    this.user.getlistClientGroup(grupId).subscribe(
      (response) => { this.listClient = response; },
      (response) => { this.mensaje.error(response.error) },
    )
    this.user.getAccountGroup(grupId).subscribe(
      (response) => { this.account = response; },
      (response) => { this.mensaje.error(response.error) },
    )
  }

}

import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { userService } from 'src/app/core/services/user.service';
import { NotificationService } from 'src/app/shared/notifications-response/notifications-response.service';

@Component({
  selector: 'app-modal-add-account',
  templateUrl: './modal-add-account.component.html',
  styleUrls: ['./modal-add-account.component.scss']
})
export class ModalAddAccountComponent implements OnInit {

  nameModal:string = 'Agregar Cuenta';

  formAccount = this.form.group({
    id: ['', [Validators.required, Validators.minLength(5), Validators.pattern('[0-9]{5,10}')]],
    monto: ['', [Validators.required, Validators.minLength(2), Validators.pattern('[0-9 ]{0,40}')]],
  })

  constructor(
    public dialogRef: MatDialogRef<ModalAddAccountComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private mensaje: NotificationService,
    private form: FormBuilder,
    private serviceUser: userService
  ) { }

  ngOnInit(): void {
  }

  addAccount(){
    const request = { data: this.formAccount.value };
    this.serviceUser.addAccountAGroup(this.data, request).subscribe(
      () => { this.dialogRef.close(true); },
      (response) => { this.mensaje.error(response.error.error) }
    )
  }

}

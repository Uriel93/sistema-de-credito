import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { userService } from 'src/app/core/services/user.service';
import { NotificationService } from 'src/app/shared/notifications-response/notifications-response.service';

@Component({
  selector: 'app-modals',
  templateUrl: './modals.component.html',
  styleUrls: ['./modals.component.scss']
})
export class ModalsGrupsComponent implements OnInit {

  nameModal:string = "";
  grupRepet:boolean = false;
  allGrup:any;

  formGrup = this.form.group({
    id: ['', [Validators.required, Validators.minLength(5), Validators.pattern('[a-zA-Z0-9]{5,10}')]],
    nombre: ['', [Validators.required, Validators.minLength(2), Validators.pattern('[a-zA-Z ]{0,40}')]],
  })

  constructor(
    public dialogRef: MatDialogRef<ModalsGrupsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private mensaje: NotificationService,
    private form: FormBuilder,
    private serviceUser: userService
  ) { }

  ngOnInit(): void {
    this.catalog();
    if(this.data){
      this.nameModal = "Editar grupo";
      this.setform(this.data);
    } else {
      this.nameModal = "Crear grupo";
    }
  }

  enviarGrup(){
    const request = { data : { ...this.formGrup.value, 
      id: (this.formGrup.value.id).toUpperCase(),
      nombre: (this.formGrup.value.nombre).toUpperCase()
    }}
    if(!this.data) {
      this.seveGrup(request)
    } else {
      this.updateGrup(request);
    }
  }

  setform(data:any){
    this.formGrup.patchValue({
      id: data.id,
      nombre: data.nombre
    })
  }

  seveGrup(request:any){
    this.grupRepet =  false;
      this.allGrup.map((id:any) => {
        if((id.id).toUpperCase() === (this.formGrup.value.id).toUpperCase()){
          this.grupRepet =  true;
        }
      })
      setTimeout(() => {
        this.grupRepet ? this.mensaje.warning("El cliente ya exite.") : this.saveClient(request)
      }, 500)
  } 

  updateGrup(request:any){
    if((this.data.id).toUpperCase() !== (this.formGrup.value.id).toUpperCase()){
      this.mensaje.warning("El id ingresado es diferente al grupo que deseas actualizar.")
    } else{
      this.updateClient(request)
    }
  }

  updateClient(request:any){
    this.serviceUser.updateGrup(request).subscribe(
      () => { this.mensaje.succes("Se actualizo con exito"); this.dialogRef.close(true); },
      (response) => { this.mensaje.error(response.error) }
    )
  }

  saveClient(request:any){
    this.serviceUser.saveGrup(request).subscribe(
      () => { this.mensaje.succes("Se guardo con exito"); this.dialogRef.close(true); },
      (response) => { this.mensaje.error(response.error) }
    )
  }

  catalog(){
    this.serviceUser.getGroup().subscribe(data => {
      this.allGrup = data;
    })
  }
  

}

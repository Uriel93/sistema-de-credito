import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { userService } from 'src/app/core/services/user.service';
import { NotificationService } from 'src/app/shared/notifications-response/notifications-response.service';

@Component({
  selector: 'app-modals',
  templateUrl: './modals.component.html',
  styleUrls: ['./modals.component.scss']
})
export class ModalsClientComponent implements OnInit {

  nameModal:string = "";
  userRepet:boolean = false;
  allCliente:any;

  formUser = this.form.group({
    id: ['', [Validators.required, Validators.minLength(7), Validators.pattern('[a-zA-Z0-9]{7,10}')]],
    nombre: ['', [Validators.required, Validators.minLength(2), Validators.pattern('[a-zA-Z ]{0,40}')]],
  })

  constructor(
    public dialogRef: MatDialogRef<ModalsClientComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private mensaje: NotificationService,
    private form: FormBuilder,
    private serviceUser: userService
  ) { }

  ngOnInit(): void {
    this.catalog();
    if(this.data){
      this.nameModal = "Editar cliente";
      this.setform(this.data);
    } else {
      this.nameModal = "Crear cliente";
    }
  }

  setform(data:any){
    this.formUser.patchValue({
      id: data.id,
      nombre: data.nombre
    })
  }

  enviarUser(){
    const request = { data : { ...this.formUser.value, 
      id: (this.formUser.value.id).toUpperCase(),
      nombre: (this.formUser.value.nombre).toUpperCase()
    }}
    if(!this.data) {
      this.seveUser(request)
    } else {
      this.updateUser(request);
    }
  }

  seveUser(request:any){
    this.userRepet =  false;
      this.allCliente.map((id:any) => {
        if((id.id).toUpperCase() === (this.formUser.value.id).toUpperCase()){
          this.userRepet =  true;
        }
      })
      setTimeout(() => {
        this.userRepet ? this.mensaje.warning("El cliente ya exite.") : this.saveClient(request)
      }, 500)
  } 

  updateUser(request:any){
    if((this.data.id).toUpperCase() !== (this.formUser.value.id).toUpperCase()){
      this.mensaje.warning("El id ingresado es diferente al cliente que deseas actualizar.")
    } else{
      this.updateClient(request)
    }
  }

  updateClient(request:any){
    this.serviceUser.updateUser(request).subscribe(
      () => { this.mensaje.succes("Se actualizo con exito"); this.dialogRef.close(true); },
      (response) => { this.mensaje.error(response.error) }
    )
  }

  saveClient(request:any){
    this.serviceUser.saveUser(request).subscribe(
      () => { this.mensaje.succes("Se guardo con exito"); this.dialogRef.close(true); },
      (response) => { this.mensaje.error(response.error) }
    )
  }

  catalog(){
    this.serviceUser.getUsers().subscribe(data => {
      this.allCliente = data;
    })
  }

}

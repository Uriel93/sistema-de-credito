import { Component, OnInit } from '@angular/core';
import { PeriodicElement } from 'src/app/core/interfaces/table-user.interface';
import { userService } from 'src/app/core/services/user.service';
import { MatDialog } from '@angular/material/dialog';
import { NotificationService } from 'src/app/shared/notifications-response/notifications-response.service';
import { ModalsClientComponent } from '../client/modals-client/modals.component';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})

export class ClientComponent implements OnInit {

  dataSource: PeriodicElement[] = [];

  displayedColumns: string[] = ['id', 'nombre', 'opciones'];

  constructor(
    private user:userService,
    private mensaje: NotificationService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.allUsers();
  }

  createUser(){
    const dialogRef = this.dialog.open(ModalsClientComponent);
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.allUsers();
      }
    });
  }

  editUser(user:any){
    const dialogRef = this.dialog.open(ModalsClientComponent, {
      data: user
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.allUsers();
      }
    });
  }

  allUsers(){
    this.user.getUsers().subscribe(
      (response) => { this.dataSource = response },
      (response) => { this.mensaje.error(response.error) }
    )
  }

}

import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private toastr: ToastrService) { }

  succes(message: string): any {
    this.toastr.success(message);
  }

  information(message: string): any {
    this.toastr.info(message);
  }

  warning(message: string): any {
    this.toastr.warning(message);
  }

  error(message: string): any {
    this.toastr.error(message);
  }

}
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { userService } from 'src/app/core/services/user.service';
import { NotificationService } from '../notifications-response/notifications-response.service';

@Component({
  selector: 'app-modal-payment',
  templateUrl: './modal-payment.component.html',
  styleUrls: ['./modal-payment.component.scss']
})
export class ModalPaymentComponent implements OnInit {

  nameModal:string = 'Pagar en la cuenta de ' + this.data.cuenta;

  formPayment = this.form.group({
    monto: ['', [Validators.required, Validators.minLength(2), Validators.pattern('[0-9 ]{0,40}')]],
  })

  constructor(
    public dialogRef: MatDialogRef<ModalPaymentComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private mensaje: NotificationService,
    private form: FormBuilder,
    private serviceUser: userService
  ) { }

  ngOnInit(): void {  }

  addPayment(){
    const request = { data: { monto: this.formPayment.value.monto } }
    this.serviceUser.payAccount(this.data.cuenta, request).subscribe(
      () => { this.dialogRef.close(true) },
      (response) => { this.mensaje.error(response.error.error) }
    )
  }

}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './core/views/login/login.component';
import { HomeComponent } from './core/views/home/home.component';

// Animación y mensaje de reponse
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

//Formulario reactivo
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

//Material
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { WelcomeComponent } from './features/welcome/welcome.component';
import { ClientComponent } from './features/client/client.component';
import { GrupsComponent } from './features/grups/grups.component';
import { MatTableModule } from '@angular/material/table';
import { AccountsComponent } from './features/accounts/accounts.component';
import { MatDialogModule } from '@angular/material/dialog';
import { ModalsClientComponent } from './features/client/modals-client/modals.component';
import { ModalsGrupsComponent } from './features/grups/modals-grup/modals.component';
import { MatListModule } from '@angular/material/list';
import { MatTabsModule } from '@angular/material/tabs';
import { ModalAddClientComponent } from './features/grups/modal-add-client/modal-add-client.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { ModalAddAccountComponent } from './features/grups/modal-add-account/modal-add-account.component';
import { ModalPaymentComponent } from './shared/modal-payment/modal-payment.component';
import { ModalCalendarComponent } from './features/grups/modal-calendar/modal-calendar.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    WelcomeComponent,
    ClientComponent,
    GrupsComponent,
    AccountsComponent,
    ModalsClientComponent,
    ModalsGrupsComponent,
    ModalAddClientComponent,
    ModalAddAccountComponent,
    ModalPaymentComponent,
    ModalCalendarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,

    //Material
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatSidenavModule,
    MatTableModule,
    MatDialogModule,
    MatListModule,
    MatTabsModule,
    MatCheckboxModule,

    //Formulario reactivo
    FormsModule,
    ReactiveFormsModule,

    // Animación y mensaje de reponse
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 6000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

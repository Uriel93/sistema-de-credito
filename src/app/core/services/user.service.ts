import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { properties } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class userService {

  private readonly url = properties.podemosApi.url + properties.podemosApi.baseContext;

  constructor(private http: HttpClient) { }

  /* clientes */
  getUsers(): Observable<any>{
    return this.http.get(this.url + "/clientes");
  }

  saveUser(user:any): Observable<any>{
    return this.http.post(this.url + "/clientes/new", user)
  }

  updateUser(user:any): Observable<any>{
    return this.http.post(this.url + "/clientes/new", user)
  }
  /* clientes */

  /* grupos */
  getGroup(): Observable<any>{
    return this.http.get(this.url + "/grupos");
  }

  getlistClientGroup(idGroup:any): Observable<any>{
    return this.http.get(this.url + "/grupos/" + idGroup + "/miembros");
  }

  getAccountGroup(idGroup:any):Observable<any>{
    return this.http.get(this.url + "/grupos/" + idGroup + "/cuentas");
  }

  saveGrup(grup:any): Observable<any>{
    return this.http.post(this.url + "/grupos/new", grup)
  }

  updateGrup(grup:any): Observable<any>{
    return this.http.post(this.url + "/grupos/new", grup)
  }

  addClientAGroup(grupId:any, request:any): Observable<any>{
    return this.http.post(this.url + "/grupos/" + grupId +"/miembros/new", request)
  }

  addAccountAGroup(grupId:any, request:any): Observable<any>{
    return this.http.post(this.url + "/grupos/" + grupId + "/cuentas/new", request)
  }
  /* grupos */

  /* cuentas */
  getAllAccount(){
    return this.http.get(this.url + "/cuentas");
  }

  listCalendar(accountId:any): Observable<any>{
    return this.http.get(this.url + "/cuentas/" + accountId + "/calendariopagos")
  }

  lisTrasacition(calendarId:any): Observable<any>{
    return this.http.get(this.url + "/cuentas/" + calendarId + "/transacciones")
  }

  payAccount(accountId:any, request:any): Observable<any>{
    return this.http.post(this.url + "/cuentas/" + accountId + "/payment", request)
  }

  /* cuentas */  
  
}
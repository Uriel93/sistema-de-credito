
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { properties } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private readonly url = properties.login.url;

  constructor(private http: HttpClient) { }

  login(request:any): Observable<any>{
    return this.http.post(this.url + "login", request);
  }
  
}
import { Component, OnInit } from '@angular/core';
import { NotificationService } from 'src/app/shared/notifications-response/notifications-response.service';
import {FormBuilder, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../../services/login.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  hide = true;
  formLogin = this.form.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required]],
  })
  
  constructor(
    private mensaje: NotificationService,
    private form: FormBuilder,
    private serviceLogin: LoginService,
    private router: Router
   ) { }

  ngOnInit(): void {
  }

  enviar(){
    this.serviceLogin.login(this.formLogin.value).subscribe(
      () => { this.router.navigate(['systemcredit']) },
      () => { this.mensaje.error("Datos incorrectos intente de nuevo.") }
    )
  }

}

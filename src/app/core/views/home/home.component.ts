import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  welcome:boolean = true;
  client:boolean = false;
  grups:boolean = false;
  account:boolean = false;

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  logOut(){
    this.router.navigate(['login'])
  }

  showClient(){
    this.welcome = false;
    this.grups = false;
    this.account = false;
    this.client = true;
  }

  showGroup(){
    this.welcome = false;
    this.client = false;
    this.account = false;
    this.grups = true;
  }

  shoWelcome(){
    this.client = false;
    this.grups = false;
    this.account = false;
    this.welcome = true;
  }

  showAccounts(){
    this.client = false;
    this.grups = false;
    this.welcome = false;
    this.account = true;
  }

}

//
//

- APLICACION: "System Credit"
- DESARROLLO: Angular CLI: 11.1.2 y Node: 10.16.0
- EXTENCIONES ANGULAR:
1. Material Design - Maquetación
2. ngx-toastr - Mensaje de response
3. cdk
- CREADO: Ing. Abraham Uriel Zavala Garcia
- PARA: Podemos Progresar
- TIEMPO DESARROLLO: 7 dias
//
//

Para poder arrancar el sistema:

1) Se debe clonar el repositorio.
2) Entrar hasta la carpeta "sistem-de-credito" por consola.
3) Se debe ingresar un **npm install** esto es para instalar todas las dependencias que se utilizaron.
4) Se debe ingresar un **npm start** esto es para que se ejecute el proxy y emitir el CORS.
5) Revisar que se levanto en el puerto **4200**
6) Listo terminamos.

Descripción del proyecto

**NOTA:**
El sistema tiene un **login que tiene user: eve.holt@reqres.in psw: cityslicka**
**OJO:** Si no ingresas con ese usuario te mandara error ya que se esta consumiendo un servicio "FAKE"

Una vez entrando encontraras la pagina de Bienvenida donde tendras un menú que esta constituido de 3 apartados

  1. Clientes 
     - Todos los clientes
     - Alta de clientes 
     - Editar clientes
  2. Grupos
      - Todos los grupos
      - Alta de grupos
      - Editar grupos
      - Listado de miembros de cada grupo
      - Agregar miembros a cada grupo
      - Listado de cuentas de cada grupo
      - En listado de cuentas tienes un boton de calendario de pagos del grupo
      - En el caldendario de pagos puedes pagar mediante una trasacción
      - Crear cuentas para un grupo
  3. Cuentas
      - Todas la cuentas
      - Calendario de pagos del grupo
      - En el caldendario de pagos puedes pagar mediante una trasacción
      - Transacciones de la cuenta

 Es proyecto bastante completo con validación en front 

 Si mas quedo atento.
